# BWR Project

## Installation
### Dependencies

`sudo apt install libsqlite3-dev`
```
cd application
mkdir build && cd build
cmake ..
make
```

## Summary
The Server accepts `Command` messages from the frontend and passes them to the corresponding robot (determined by the `targetId` field of the command).
Robots respond with `CommandResult` which indicates whether the command has been accepted or not.
Possible reasons for the robot to reject the command are: 
* `Start` command:
  * The task is already running
  * Bad mood (every 10th second) [^1]
* `Stop` command:
  * The desired task is not running

The server will pass commands only to the active robots (`KeepAlive` message in last 10 seconds).

During the task execution, the robot will send periodic `TaskStatus` updates, which include the `taskId`, `commandId` that started the task, and the progress of the task.

[^1]: This meant to add some random reasons for the robot to reject a command.

## Usage
Open in separate terminals:
* `./server_executable`
* `./robot_executable`

Specifying robot ID and server host is done by providing the executable arguments in the following format: `./robot_executable $ID HOST`

For example:
```
./robot_executable 25 192.168.0.100
```


### Example requests:
#### See Robots list
GET: `http://127.0.0.1:22000/robots`

#### Check robot's state
GET: `http://127.0.0.1:22000/robot?id=Bobby1`

#### Command a robot to start a task
PUT: `http://127.0.0.1:22000/command`
with `application/json` body: 
```
{
	"metadata": {
		"id": "1fa2",
		"targetId": "Bobby1",
		"sourceId": "insomnia"
	},
	"type": "Start",
	"taskId": "t5"
}
```
