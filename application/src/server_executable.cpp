//
// Created by yossi on 2021-04-29.
//
#include <iostream>
#include <csignal>
#include "Server/server.hpp"

std::function<bool(int)> sigint_wrapper;

void sigint_handler(int signum) {
    if (sigint_wrapper(signum)) {
        exit(0);
    };
    exit(signum);
}

int main(int argc, char **argv) {
    Server server;
    for (int i = 0; i < argc; ++i) {
        if (std::string(argv[i]) == "debug") {
            server.console_logger->set_level(spdlog::level::debug);
        }
    }
    sigint_wrapper = [&server](int signum) {
        server.console_logger->critical("Ctrl-C pressed: Stopping the server");
        server.stop();
        return true;
    };

    struct sigaction sigIntHandler{};
    sigIntHandler.sa_handler = sigint_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGTERM, &sigIntHandler, nullptr);
    sigaction(SIGINT, &sigIntHandler, nullptr);

    server.setup();
    server.start();
    server.console_logger->info("Shut down correctly");
    return 0;
}
