//
// Created by yossi on 2021-04-29.
//

#include <iostream>

#include <csignal>

#include "Robot/robot.hpp"

std::function<bool(int)> sigint_wrapper;

void sigint_handler(int signum) {
    if (sigint_wrapper(signum)) {
        exit(0);
    };
    exit(signum);
}

int main(int argc, char **argv) {
    int robot_number = 0;
    if (argc > 1) {
        robot_number = strtol(argv[1], nullptr, 10);
    }
    if (robot_number == 0) {
        robot_number = 1;
    }
    std::string host = "localhost";
    if (argc > 2) {
        host = argv[2];
    }
    Robot robot(robot_number, host);
    sigint_wrapper = [&robot](int signum) {
        robot.console_logger->critical("Ctrl-C pressed: Stopping the robot");
        robot.stop();
        return true;
    };

    struct sigaction sigIntHandler{};
    sigIntHandler.sa_handler = sigint_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGTERM, &sigIntHandler, nullptr);
    sigaction(SIGINT, &sigIntHandler, nullptr);

    robot.run();
    robot.console_logger->warn("Shut down correctly");
    return 0;
}
