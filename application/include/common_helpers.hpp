//
// Created by yossi on 2021-04-29.
//

#pragma once

#include <chrono>
#include <string>
#include "ThirdParty/nlohmann/json.hpp"

namespace common_helpers {
    inline double time_now() {
        auto current_time = std::chrono::system_clock::now();
        auto duration_in_seconds = std::chrono::duration<double>(current_time.time_since_epoch());

        return duration_in_seconds.count();
    }

    //using json = nlohmann::json;
    //using namespace std::string_literals;

    inline void
    ensureObjectHasKeys(const std::string &object_name, const nlohmann::json &json_object,
                        const std::vector<std::string> &keys) {
        for (const auto &key: keys) {
            if (not json_object.contains(key)) {
                char buff[256];
                sprintf(buff, "%s missing '%s' key.", object_name.c_str(), key.c_str());
                throw std::runtime_error(buff);
            }
        }
    }
}
