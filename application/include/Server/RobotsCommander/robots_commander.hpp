//
// Created by yossi on 2021-04-30.
//

#pragma once

#include <string>
#include <map>
#include <httplib.h>

#include "DataTypes/Command.hpp"
#include "DataTypes/CommandResult.hpp"
#include "ThirdParty/spdlog/spdlog.h"

class RobotsCommander {
public:
    explicit RobotsCommander(std::shared_ptr<spdlog::logger> logger_ptr);

    void addRobotIfDoesntExist(const std::string &robot_id, std::string remote_address = "");

    bool passCommandToRobot(const std::string &robot_id, const Command &command, CommandResult &result);

private:
    std::map<std::string, std::unique_ptr<httplib::Client>> clients;
    std::shared_ptr<spdlog::logger> logger;
};


