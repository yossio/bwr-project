//
// Created by yossi on 2021-04-30.
//

#include "robots_commander.hpp"

#include <utility>


RobotsCommander::RobotsCommander(std::shared_ptr<spdlog::logger> logger_ptr)
        : logger(std::move(logger_ptr)) {}

void RobotsCommander::addRobotIfDoesntExist(const std::string &robot_id, std::string remote_address) {

    if (clients.count(robot_id)) {
        return;
    }
    std::string digits = std::regex_replace(robot_id.c_str(), std::regex(R"([\D])"), "");
    int robot_num = (int) std::strtol(digits.c_str(), nullptr, 10);
    if (robot_num == 0) {
        throw std::runtime_error("Cannot deduct robot number from name: " + robot_id);
    }
    int port = 22200 + robot_num;
    clients[robot_id] = std::make_unique<httplib::Client>(remote_address.empty() ? "localhost" : remote_address, port);
    logger->debug("RobotsCommander {} added on port: {}", robot_id, port);
}

bool RobotsCommander::passCommandToRobot(const std::string &robot_id, const Command &command, CommandResult &result) {
    auto response = clients[robot_id]->Put("/command", command.to_json().dump(), "application/json");
    switch (response.error()) {
        case httplib::Error::Success:
            logger->info("Successfully forwarded the command to the robot");
            result = CommandResult::from_json_string(response->body);
            break;
        case httplib::Error::Connection:
            result.message += "Failed to forward the command to the robot - Connection issue";
            result.result = CommandResult::E_REJECTED;
            logger->error("Failed to forward the command to the robot - Connection issue");
            break;
        default:
            result.message += "Failed to forward the command to the robot - Unknown issue";
            result.result = CommandResult::E_REJECTED;
            logger->error("Tried to forward the command to the robot, but failed :(");
            return false;

    }
    result.message = "Passed through server; " + result.message;
    return true;
}
