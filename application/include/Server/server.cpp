//
// Created by yossi on 2021-04-29.
//

#include "server.hpp"

#include <chrono>
#include <thread>
#include <iostream>

#include "Database/sq_lite_db.hpp"
#include "RestServer/httplib_rest_server.hpp"
#include "common_helpers.hpp"

using namespace std::chrono_literals;
using namespace std::string_literals;

Server::Server() {
    console_logger = spdlog::stdout_color_mt("Server");
}

Server::~Server() {
    stop();
}

void Server::setup() {
    storage = std::make_shared<SQLiteDB>(console_logger);

    robots_commander = std::make_unique<RobotsCommander>(console_logger);

    robots_keeper = std::make_unique<RobotsKeeper>(console_logger, storage);
    robots_keeper->setup();

    setupRestServer();
    console_logger->debug("Setup complete");
}

void Server::setupRestServer() {
    rest_server = std::make_unique<httplibRestServer>();
    rest_server->setupServer(host_, port_);
    rest_server->setupKeepAliveEndpoint(
            "/keep_alive",
            [=](const KeepAlive &msg, std::string remote_address) { return processKeepAlive(msg, remote_address); });
    rest_server->setupRobotsEndpoint(
            "/robots",
            [=](Robots &robots) {
                robots.metadata.source_id = "server";
                return reportRobots(robots);
            });
    rest_server->setupRobotStateEndpoint(
            "/robot",
            [=](const std::string &robot_id, RobotState &state) {
                return robots_keeper->getRobotState(robot_id, state);
            });
    rest_server->setupTaskStatusEndpoint(
            "/task_status",
            [=](const TaskStatus &task_status) { processTaskStatus(task_status); });
    rest_server->setupCommandEndpoint(
            "/command",
            [=](const Command &command, CommandResult &result) { return processCommand(command, result); });
}

void Server::processTaskStatus(const TaskStatus &task_status) {
    std::string msg_details = task_status.message.empty() ? "" :
                              "[" + task_status.message + "]";
    console_logger->info("<{}> TaskStatus: <{}> is {} {}",
                         task_status.metadata.source_id.c_str(),
                         task_status.taskId.c_str(),
                         task_status.get_status_string().c_str(),
                         msg_details.c_str());
}

bool Server::processKeepAlive(const KeepAlive &msg, std::string remote_address) {
    double now = common_helpers::time_now();
    std::string robot_id = msg.metadata.source_id;
    double delay = now - msg.unix_timestamp;
    bool ok = true;
    if (std::abs(delay) > 2.0) {
        console_logger->warn("Keep-alive timestamp from <{}> has delay of {} seconds - Ignoring.", robot_id.c_str(),
                             delay);
        ok = false;
    } else {
        storage->logKeepAliveMsg(msg);
        robots_keeper->processKeepAliveMsg(msg, now);
        robots_commander->addRobotIfDoesntExist(msg.metadata.source_id, remote_address);
    }
    return ok;
}

bool Server::processCommand(const Command &command, CommandResult &result) {
    storage->logCommandMsg(command);
    result.commandId = command.metadata.id;
    std::string target_id = command.metadata.target_id;
    if (robots_keeper->isRobotOn(target_id)) {
        console_logger->debug("Forwarding the command to robot {}", target_id.c_str());
        robots_commander->passCommandToRobot(target_id, command, result);
        storage->logCommandResultMsg(result);
        return true;
    } else {
        result.message = "Requested robot is not available";
        result.result = CommandResult::E_REJECTED;
        return false;
    }
}

bool Server::reportRobots(Robots &robots) {
    for (const auto &robot_id: robots_keeper->getRobotsList()) {
        robots.ids.emplace_back(robot_id);
    }
    return not robots.ids.empty();
}

void Server::start() {
    rest_server->start();
    is_running = true;
    main_thread = std::thread([=]() {
        console_logger->debug("Started run() loop");
        while (is_running) {
            std::this_thread::sleep_for(100ms);
            robots_keeper->checkAndSaveRobotStates(common_helpers::time_now());
        }
        console_logger->debug("Done run() loop");
    });
    console_logger->info("Started server at " + host_ + ":" + std::to_string(port_));
    main_thread.join();
}

void Server::stop() {
    if (is_running) {
        is_running = false;
        rest_server->stop();
        main_thread.join();
        console_logger->warn("Server stopped.");
    } else {
        console_logger->warn("Server is not running - nothing to stop.");
    }
}
