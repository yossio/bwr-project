//
// Created by yossi on 2021-04-29.
//

#include <utility>

#include "robots_keeper.hpp"

RobotsKeeper::RobotsKeeper(std::shared_ptr<spdlog::logger> logger_ptr,
                           std::shared_ptr<DataBaseInterface> storage_ptr)
        : logger(std::move(logger_ptr)), storage(std::move(storage_ptr)) {
}


void RobotsKeeper::processKeepAliveMsg(const KeepAlive &msg, double now) {
    std::string robot_id = msg.metadata.source_id;
    if (robot_states.count(robot_id)) {
        updateRobotState(msg, now, robot_id);
    } else {
        emplaceRobotState(robot_id, msg);
        logger->info("Starting to receive keep-alive from '{}'", robot_id.c_str());
    }
}

void RobotsKeeper::updateRobotState(const KeepAlive &msg, double now, const std::string &robot_id) {
    RobotState &robot_state = robot_states[robot_id];
    robot_state.metadata = msg.metadata;
    robot_state.unix_timestamp = now;
}

void RobotsKeeper::emplaceRobotState(const std::string &robot_id, const KeepAlive &msg) {
    RobotState robot_state;
    robot_state.state = RobotState::E_ON;
    robot_state.metadata = msg.metadata;
    robot_state.unix_timestamp = msg.unix_timestamp;
    robot_states[robot_id] = robot_state;
}

void RobotsKeeper::checkAndSaveRobotStates(double now) {
    for (auto&[robot_id, robot_state]: robot_states) {
        if (now - robot_state.unix_timestamp <= 10) {
            if (robot_state.state != RobotState::E_ON) {
                robot_state.state = RobotState::E_ON;
                logger->info("Keep-alive from <{}> restored", robot_id.c_str());
            }
        } else {
            if (robot_state.state != RobotState::E_OFF) {
                robot_state.state = RobotState::E_OFF;
                logger->warn("Keep-alive from <{}> lost", robot_id.c_str());
            }
        }
        storage->saveRobotState(robot_state);
    }
}

std::vector<std::string> RobotsKeeper::getRobotsList() {
    std::vector<std::string> robots;
    for (const auto &robot: robot_states) {
        robots.emplace_back(robot.first);
    }
    return robots;
}

bool RobotsKeeper::getRobotState(const std::string &id, RobotState &state) {
    if (robot_states.count(id)) {
        state = robot_states.at(id);
        return true;
    } else {
        return false;
    }
}

bool RobotsKeeper::isRobotOn(const std::string &id) {
    RobotState state;
    bool ok = getRobotState(id, state);
    return ok and state.state == RobotState::E_ON;
}

void RobotsKeeper::loadRobotsFromDb() {
    logger->debug("Loading Robots from DB");
    for (const auto &robot_state: storage->getRobotStates()) {
        robot_states[robot_state.metadata.source_id] = robot_state;
    }
    logger->debug("Loaded {} robot states from DB", robot_states.size());
}

void RobotsKeeper::setup() {
    loadRobotsFromDb();
}
