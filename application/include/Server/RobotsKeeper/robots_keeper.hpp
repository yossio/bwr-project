//
// Created by yossi on 2021-04-29.
//

#pragma once

#include <string>

#include "ThirdParty/spdlog/spdlog.h"

#include "DataTypes/KeepAlive.hpp"
#include "DataTypes/RobotState.hpp"
#include "Server/Database/data_base_interface.hpp"

class Server;

class RobotsKeeper {
public:
    RobotsKeeper(std::shared_ptr<spdlog::logger> logger_ptr, std::shared_ptr<DataBaseInterface> storage_ptr);

    void setup();

    void loadRobotsFromDb();

    void updateRobotState(const KeepAlive &msg, double now, const std::string &robot_id);

    void emplaceRobotState(const std::string &robot_id, const KeepAlive &msg);

    void checkAndSaveRobotStates(double now);

    void processKeepAliveMsg(const KeepAlive &msg, double now);

    std::vector<std::string> getRobotsList();

    bool getRobotState(const std::string &id, RobotState &state);

    bool isRobotOn(const std::string &id);

private:
    std::shared_ptr<spdlog::logger> logger;
    std::shared_ptr<DataBaseInterface> storage;
    std::map<std::string, RobotState> robot_states;
};


