//
// Created by yossi on 2021-04-28.
//

#include "httplib_rest_server.hpp"

#include <utility>

#include "ThirdParty/nlohmann/json.hpp"


bool httplibRestServer::setupServer(std::string host, int port) {
    host_ = std::move(host);
    port_ = port;
    return true;
}

void httplibRestServer::start() {
    svr_thread = std::thread([=]() {
        svr.listen(host_.c_str(), port_);
    });
}

void httplibRestServer::stop() {
    svr.stop();
    svr_thread.join();
}

void httplibRestServer::setupKeepAliveEndpoint(std::string endpoint, std::function<bool(KeepAlive, std::string)> callback) {
    svr.Post(endpoint.c_str(), [=](const httplib::Request &req, httplib::Response &res) {
        if (req.body.empty()) {
            res.status = 400;
            res.set_content("I got an empty message", "text/plain");
            return;
        }
        auto keep_alive_msg = KeepAlive::from_json_string(req.body);
        if (callback(keep_alive_msg, req.remote_addr)) {
            res.status = 202;
            res.set_content("OK", "text/plain");
        } else {
            res.status = 400;
            res.set_content("Timestamp from the robot is different from the current time.", "text/plain");
        }

    });
}

void httplibRestServer::setupRobotsEndpoint(std::string endpoint, std::function<bool(Robots &robots)> callback) {
    svr.Get(endpoint.c_str(), [=](const httplib::Request &req, httplib::Response &res) {
        Robots robots;
        if (callback(robots)) {
            res.status = 200;
            res.set_content(robots.to_json().dump(), "application/json");
        } else {
            res.status = 404;
            res.set_content("Not found any robots", "text/plain");
        }
    });
}

void
httplibRestServer::setupRobotStateEndpoint(std::string endpoint, std::function<bool(std::string, RobotState &)> callback) {
    svr.Get(endpoint.c_str(), [=](const httplib::Request &req, httplib::Response &res) {
        if (not req.has_param("id")) {
            res.set_redirect("/robots");
        }
        RobotState robot_state;
        if (callback(req.get_param_value("id"), robot_state)) {
            res.status = 200;
            res.set_content(robot_state.to_json().dump(), "application/json");
        } else {
            res.status = 404;
            res.set_content("Not found any robots", "text/plain");
        }
    });
}

void httplibRestServer::setupTaskStatusEndpoint(std::string endpoint, std::function<void(TaskStatus)> callback) {
    svr.Post(endpoint.c_str(), [=](const httplib::Request &req, httplib::Response &res) {
        if (req.body.empty()) {
            res.status = 400;
            res.set_content("I got an empty message", "text/plain");
            return;
        }
        auto task_status = TaskStatus::from_json_string(req.body);
        callback(task_status);
        res.status = 202;
        res.set_content("Received TaskUpdate", "text/plain");
    });

}

void httplibRestServer::setupCommandEndpoint(std::string endpoint, std::function<bool(Command, CommandResult &)> callback) {
    svr.Put(endpoint.c_str(), [=](const httplib::Request &req, httplib::Response &res) {
        if (req.body.empty()) {
            res.status = 400;
            res.set_content("I got an empty message", "text/plain");
            return;
        }
        auto command = Command::from_json_string(req.body);
        CommandResult result;
        if (callback(command, result)) {
            res.status = 202;
        } else {
            res.status = 400;
        }
        res.set_content(result.to_json().dump(), "application/json");

    });
}
