//
// Created by yossi on 2021-04-28.
//

#pragma once

#include <thread>

#include "rest_server_interface.hpp"

#include "ThirdParty/httplib.h"

class httplibRestServer : public RestServerInterface {
public:
    virtual ~httplibRestServer() = default;

    bool setupServer(std::string host, int port) override;

    void start() override;

    void stop() override;

    void setupKeepAliveEndpoint(std::string endpoint, std::function<bool(KeepAlive, std::string)> callback) override;

    void setupRobotsEndpoint(std::string endpoint, std::function<bool(Robots &)> callback) override;

    void
    setupRobotStateEndpoint(std::string endpoint, std::function<bool(std::string, RobotState &)> callback) override;

    void setupTaskStatusEndpoint(std::string endpoint, std::function<void(TaskStatus)> callback) override;

    void setupCommandEndpoint(std::string endpoint, std::function<bool(Command, CommandResult &)> callback) override;

private:
    httplib::Server svr;
    //std::unique_ptr<httplib::Server> svr;
    std::string host_;
    int port_;
    std::thread svr_thread;
};


