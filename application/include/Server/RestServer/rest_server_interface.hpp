//
// Created by yossi on 2021-04-29.
//

#pragma once

#include <functional>

#include "DataTypes/KeepAlive.hpp"
#include "DataTypes/Command.hpp"
#include "DataTypes/CommandResult.hpp"
#include "DataTypes/Robots.hpp"
#include "DataTypes/RobotState.hpp"
#include "DataTypes/TaskStatus.hpp"

class RestServerInterface {
public:
    virtual bool setupServer(std::string host, int port) = 0;

    virtual void start() = 0;

    virtual void stop() = 0;

    virtual void setupKeepAliveEndpoint(std::string endpoint, std::function<bool(KeepAlive, std::string)> callback) = 0;

    virtual void
    setupRobotsEndpoint(std::string endpoint, std::function<bool(Robots &)> callback) = 0;

    virtual void
    setupRobotStateEndpoint(std::string endpoint, std::function<bool(std::string robot_id, RobotState &)> callback) = 0;

    virtual void setupTaskStatusEndpoint(std::string endpoint, std::function<void(TaskStatus)> callback) = 0;

    virtual void setupCommandEndpoint(std::string endpoint, std::function<bool(Command, CommandResult &)> callback) = 0;
};


