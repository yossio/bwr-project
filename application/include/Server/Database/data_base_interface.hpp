//
// Created by yossi on 2021-04-28.
//

#pragma once

#include "DataTypes/MetaData.hpp"
#include "DataTypes/KeepAlive.hpp"
#include "DataTypes/Command.hpp"
#include "DataTypes/CommandResult.hpp"
#include "DataTypes/RobotState.hpp"

class DataBaseInterface {
public:
    virtual void logKeepAliveMsg(KeepAlive msg) = 0;

    virtual void logCommandMsg(Command msg) = 0;

    virtual void logCommandResultMsg(CommandResult msg) = 0;

    virtual std::vector<RobotState> getRobotStates() = 0;

    virtual void saveRobotState(RobotState robotState) = 0;
};


