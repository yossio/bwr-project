//
// Created by yossi on 2021-04-28.
//

#include "sq_lite_db.hpp"

#include <utility>

SQLiteDB::SQLiteDB(std::shared_ptr<spdlog::logger> logger, bool logKeepAliveMsgs)
        : logger(std::move(logger)),
          log_keep_alive_msgs(logKeepAliveMsgs) {
    storage = std::make_unique<SQLiteStorage>(initStorage("db.sqlite"));
    storage->sync_schema();
}


void SQLiteDB::logKeepAliveMsg(KeepAlive msg) {
    if (log_keep_alive_msgs) {
        if (logger) { logger->debug("Called logKeepAliveMsg(..) {}", msg.to_json().dump()); }
        storage->insert(KeepAliveDB(msg));
    }
}

void SQLiteDB::logCommandMsg(Command msg) {
    if (logger) { logger->debug("Called logCommandMsg(..) {}", msg.to_json().dump()); }
    storage->insert(CommandDB(msg));

}

void SQLiteDB::logCommandResultMsg(CommandResult msg) {
    if (logger) { logger->debug("Called logCommandResultMsg(..) {}", msg.to_json().dump()); }
    storage->insert(CommandResultDB(msg));
}

void SQLiteDB::setup() {
    if (logger) { logger->debug("SQLiteStorage Setup complete"); }
}

std::vector<RobotState> SQLiteDB::getRobotStates() {
    std::vector<RobotState> robots;
    for (auto &robot: storage->iterate<RobotStateDB>()) {
        robots.emplace_back(robot.toRobotState());
    }
    return robots;
}

void SQLiteDB::saveRobotState(RobotState robotState) {
    RobotStateDB robot_state_db(robotState);
    storage->replace(robot_state_db);
}
