//
// Created by yossi on 2021-04-28.
//

#pragma once

#include "data_base_interface.hpp"

#include "ThirdParty/sqlite_orm/sqlite_orm.h"
#include "ThirdParty/spdlog/spdlog.h"


#include "DataTypes/KeepAlive.hpp"
#include "DataTypes/Command.hpp"
#include "DataTypes/CommandResult.hpp"
#include "DataTypes/RobotState.hpp"


inline auto initStorage(const std::string &path) {
    using namespace sqlite_orm;
    return make_storage(path,
                        make_table("KEEP_ALIVE",
                                   make_column("ID", &KeepAliveDB::db_id, primary_key()),
                                   make_column("MSG_ID", &KeepAliveDB::id),
                                   make_column("SOURCE_ID", &KeepAliveDB::source_id),
                                   make_column("TARGET_ID", &KeepAliveDB::target_id),
                                   make_column("TIMESTAMP", &KeepAliveDB::unix_timestamp)),
                        make_table("ROBOTS",
                                   make_column("SOURCE_ID", &RobotStateDB::source_id, primary_key()),
                                   make_column("MSG_ID", &RobotStateDB::id),
                                   make_column("TARGET_ID", &RobotStateDB::target_id),
                                   make_column("IS_ON", &RobotStateDB::is_on)),
                        make_table("COMMANDS",
                                   make_column("ID", &CommandDB::db_id, primary_key()),
                                   make_column("MSG_ID", &CommandDB::id),
                                   make_column("SOURCE_ID", &CommandDB::source_id),
                                   make_column("TARGET_ID", &CommandDB::target_id),
                                   make_column("TYPE", &CommandDB::type),
                                   make_column("TASK_ID", &CommandDB::taskId)
                        ),
                        make_table("COMMAND_RESULTS",
                                   make_column("ID", &CommandResultDB::db_id, primary_key()),
                                   make_column("MSG_ID", &CommandResultDB::id),
                                   make_column("SOURCE_ID", &CommandResultDB::source_id),
                                   make_column("TARGET_ID", &CommandResultDB::target_id),
                                   make_column("COMMAND_ID", &CommandResultDB::commandId),
                                   make_column("RESULT", &CommandResultDB::result),
                                   make_column("MESSAGE", &CommandResultDB::message)
                        )
    );
}

using SQLiteStorage = decltype(initStorage(""));

class SQLiteDB : public DataBaseInterface {
public:
    explicit SQLiteDB(std::shared_ptr<spdlog::logger> logger, bool logKeepAliveMsgs = false);

    void setup();

    void logKeepAliveMsg(KeepAlive msg) override;

    void logCommandMsg(Command msg) override;

    void logCommandResultMsg(CommandResult msg) override;

    std::vector<RobotState> getRobotStates() override;

    void saveRobotState(RobotState robotState) override;

private:
    std::shared_ptr<spdlog::logger> logger;
    std::unique_ptr<SQLiteStorage> storage;
    bool log_keep_alive_msgs;
};
