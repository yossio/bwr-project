//
// Created by yossi on 2021-04-29.
//

#pragma once

#include <memory>

#include "ThirdParty/spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"


#include "Database/data_base_interface.hpp"
#include "RestServer/rest_server_interface.hpp"
#include "RobotsKeeper/robots_keeper.hpp"
#include "RobotsCommander/robots_commander.hpp"

class Server {
public:
    Server();

    virtual ~Server();

    void setup();

    void start();

    void stop();

    std::shared_ptr<spdlog::logger> console_logger;

private:
    std::string host_ = "0.0.0.0";
    int port_ = 22000;
    bool is_running = false;

    std::thread main_thread;

    std::shared_ptr<DataBaseInterface> storage;

    std::unique_ptr<RobotsKeeper> robots_keeper;

    std::unique_ptr<RobotsCommander> robots_commander;

    std::unique_ptr<RestServerInterface> rest_server;

    void setupRestServer();

    bool processKeepAlive(const KeepAlive &msg, std::string remote_address);

    bool reportRobots(Robots &robots);

    bool processCommand(const Command &command, CommandResult &result);

    void processTaskStatus(const TaskStatus &task_status);
};


