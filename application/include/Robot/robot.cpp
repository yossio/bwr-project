//
// Created by yossi on 2021-04-29.
//
#include <chrono>
#include <thread>
#include <string>
#include <regex>

#include "robot.hpp"

#include "common_helpers.hpp"
#include "DataTypes/Command.hpp"
#include "DataTypes/CommandResult.hpp"

using namespace std::chrono_literals;

Robot::Robot(int number, const std::string& host) :
        rest_client(host, 22000), keep_alive_msg() {
    robot_name = "Bobby" + std::to_string(number);
    console_logger = spdlog::stdout_color_mt(robot_name);
    port_ = 22200 + number;
    keep_alive_msg.metadata.target_id = "server";
    keep_alive_msg.metadata.source_id = robot_name;
    rest_server.Put("/command", [=](const httplib::Request &req, httplib::Response &res) {
        processCommand(req, res);
    });
}

void Robot::run() {
    robot_thread = std::thread([=]() {
        console_logger->info("Ready to rock and roll!");
        rest_server.listen("0.0.0.0", port_);
    });
    is_running = true;
    while (is_running) {
        std::this_thread::sleep_for(100ms);
        postKeepAlive();
    }
}

void Robot::stop() {
    is_running = false;
    rest_server.stop();
    robot_thread.join();
    console_logger->warn("Robot stopped.");
}

void Robot::postKeepAlive() {
    double now = common_helpers::time_now();
    double dt = 1.0;
    if (now - last_keep_alive_time > dt) {

        keep_alive_msg.unix_timestamp = round(now);
        keep_alive_msg.metadata.id = std::to_string(std::fmod(now, 600));
        auto response = rest_client.Post("/keep_alive",
                                         keep_alive_msg.to_json().dump(),
                                         "application/json");
        switch (response.error()) {
            case httplib::Error::Success:
                dt = 3.0;
                if (keep_alive_failing or last_keep_alive_time == 0) {
                    console_logger->info("Posting keep-alive notifications.");
                    keep_alive_failing = false;
                }
                break;
            case httplib::Error::Connection:
                dt = 1.0;
                if (not keep_alive_failing) {
                    console_logger->error("Failed to establish keep-alive connection to server");
                    keep_alive_failing = true;
                }
                break;
            default:
                dt = 1.0;
                if (not keep_alive_failing) {
                    keep_alive_failing = true;
                    console_logger->error("Tried to post keep-alive message, but failed :(");
                }
        }
        last_keep_alive_time = now;
    }
}

void Robot::processCommand(const httplib::Request &req, httplib::Response &res) {
    if (req.body.empty()) {
        res.status = 400;
        res.set_content("I got an empty message", "text/plain");
        return;
    }
    auto command = Command::from_json_string(req.body);
    CommandResult result;
    if (command.type == Command::E_START) {
        processStartCommand(command, result);

    } else if (command.type == Command::E_STOP) {
        stopTask(command.taskId, result);
    }
    result.metadata.source_id = command.metadata.target_id;
    result.metadata.target_id = command.metadata.source_id;
    result.commandId = command.metadata.id;
    const std::basic_string<char> &s = result.to_json().dump();
    res.set_content(s, "application/json");
    res.status = 202;
}

void Robot::processStartCommand(const Command &command, CommandResult &result) {
    std::string task_id = command.taskId;
    std::string reject_reason;
    bool will_accept = checkShouldAccept(task_id, result, reject_reason);
    int estimated_duration = getEstimatedDuration(task_id);

    if (will_accept) {
        tasks_status[task_id] = true;
        executeTask(command, estimated_duration);
    }
    console_logger->info(
            "Command: START '{}' ({} sec). {} it{}",
            task_id.c_str(), estimated_duration,
            will_accept ? "Accepting" : "Rejecting",
            will_accept ? "" : reject_reason.c_str());
}

int Robot::getEstimatedDuration(const std::string &task_id) {
    int estimated_duration;
    std::string task_num = std::regex_replace(task_id.c_str(), std::regex(R"([\D])"), "");
    estimated_duration = (int) strtol(task_num.c_str(), nullptr, 10);
    if (estimated_duration == 0) {
        estimated_duration = 5;
    }
    return estimated_duration;
}

bool Robot::checkShouldAccept(const std::string &task_id, CommandResult &result, std::string &reject_reason) {
    bool will_accept;
    if (tasks_status.count(task_id) and tasks_status[task_id]) {
        will_accept = false;
        reject_reason = ": This task is already running";
    } else {
        will_accept = std::fmod(common_helpers::time_now(), 10) > 1;
        reject_reason = will_accept ? "" : ": I am scratching myself, try in a second.";
        result.result = will_accept ? CommandResult::E_ACCEPTED : CommandResult::E_REJECTED;
    }
    return will_accept;
}

void Robot::executeTask(const Command &command, int estimated_duration) {
    task_threads.emplace_back(std::thread([=](const Command cmd, int duration) {
        for (int t = 0; t < duration; ++t) {
            if (t > 10) {
                reportTaskStatus(cmd, TaskStatus::E_IN_PROGRESS, "Too long, let's say it's done ;)");
                break;
            }
            if (tasks_status[cmd.taskId]) {
                double progress = 100.0 * double(t) / duration;
                reportTaskStatus(cmd, TaskStatus::E_IN_PROGRESS,
                                 std::to_string(int(progress)) + "%");
            } else {
                break;
            }
            std::this_thread::sleep_for(1000ms);
        }
        if (tasks_status[cmd.taskId]) {
            reportTaskStatus(cmd, TaskStatus::E_COMPLETED, "");
        } else {
            reportTaskStatus(cmd, TaskStatus::E_CANCELED, "");
        }
        tasks_status.erase(cmd.taskId);
    }, command, estimated_duration));
}

void Robot::stopTask(const std::string &task_id, CommandResult &result) {
    if (tasks_status[task_id]) {
        result.result = CommandResult::E_ACCEPTED;
        console_logger->info(
                "Command: STOP task '{}' - will do.", task_id.c_str());
    } else {
        result.result = CommandResult::E_REJECTED;
        console_logger->warn(
                "Command: STOP task '{}' - The task is not running.", task_id.c_str());
    }
    tasks_status[task_id] = false;
}

void Robot::reportTaskStatus(const Command &command, TaskStatus::Status status, const std::string &msg) {
    TaskStatus task_status;
    task_status.taskId = command.taskId;
    task_status.status = status;
    task_status.message = msg;
    task_status.commandId = command.metadata.id;
    task_status.metadata.id = "status: "s + command.taskId;
    task_status.metadata.source_id = robot_name;
    task_status.metadata.target_id = command.metadata.source_id;

    auto response = rest_client.Post("/task_status",
                                     task_status.to_json().dump(),
                                     "application/json");
    switch (response.error()) {
        case httplib::Error::Success:
            console_logger->info("Reporting task '{}' (cmd.id={}) {} {}", task_status.taskId.c_str(),
                                 command.metadata.id.c_str(), task_status.get_status_string().c_str(), msg.c_str());
            break;
        case httplib::Error::Connection:
            console_logger->error("Failed to report task status for '{}' (cmd.id={}) - Connection to server lost.",
                                  task_status.taskId.c_str(), command.metadata.id.c_str());
            break;
        default:
            console_logger->error("Failed to report task status for '{}' (cmd.id={}) - UNKNOWN REASON.",
                                  task_status.taskId.c_str(), command.metadata.id.c_str());
    }
}

