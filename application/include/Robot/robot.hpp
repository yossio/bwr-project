//
// Created by yossi on 2021-04-29.
//

#pragma once

#include <thread>
#include <DataTypes/Command.hpp>
#include <DataTypes/TaskStatus.hpp>
#include <DataTypes/CommandResult.hpp>

#include "spdlog/sinks/stdout_color_sinks.h"
#include "ThirdParty/httplib.h"
#include "ThirdParty/spdlog/spdlog.h"

#include "DataTypes/KeepAlive.hpp"

class Robot {

public:
    explicit Robot(int number, const std::string &host = "localhost");

    ~Robot() = default;

    void stop();

    void run();

    std::shared_ptr<spdlog::logger> console_logger;
private:
    std::string robot_name;
    KeepAlive keep_alive_msg;
    httplib::Server rest_server;
    httplib::Client rest_client;
    int port_;

    std::thread robot_thread;
    bool is_running = false;
    bool keep_alive_failing = false;

    double last_keep_alive_time{};
    std::map<std::string, bool> tasks_status;
    std::vector<std::thread> task_threads;

    void reportTaskStatus(const Command &command, TaskStatus::Status status, const std::string &msg);

    void postKeepAlive();

    void executeTask(const Command &command, int estimated_duration);

    bool
    checkShouldAccept(const std::string &task_id, CommandResult &result, std::string &reject_reason);

    void processCommand(const httplib::Request &req, httplib::Response &res);

    void stopTask(const std::string &task_id, CommandResult &result);

    void processStartCommand(const Command &command, CommandResult &result);

    static int getEstimatedDuration(const std::string &task_id);
};
