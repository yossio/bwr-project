//
// Created by yossi on 2021-04-29.
//

#pragma once

#include "MetaData.hpp"
#include "ThirdParty/nlohmann/json.hpp"

struct RobotState {
    MetaData metadata;
    double unix_timestamp{};
    enum State {
        E_ON,
        E_OFF
    } state{};

    [[nodiscard]] std::string get_state() const {
        switch (state) {
            case E_ON:
                return "On";
            case E_OFF:
                return "Off";
            default:
                return "UNKNOWN";
        }
    }

    [[nodiscard]] json to_json() const {
        json json_object;
        json_object["metadata"] = metadata.to_json();
        json_object["unix_timestamp"] = unix_timestamp;
        json_object["state"] = get_state();
        return json_object;
    }

    [[maybe_unused]] static RobotState from_json_string(std::string json_string) {
        return from_json_object(json::parse(json_string));
    }

    static RobotState from_json_object(json json_object) {
        RobotState result;
        common_helpers::ensureObjectHasKeys(
                "RobotState", json_object,
                {
                        "metadata",
                        "unix_timestamp",
                        "state",
                });

        result.metadata = MetaData::from_json_object(json_object["metadata"]);
        result.unix_timestamp = json_object["unix_timestamp"];

        if (json_object["state"] == "On") {
            result.state = E_ON;
        } else if (json_object["state"] == "Off") {
            result.state = E_OFF;
        }
        return result;
    }
};

struct RobotStateDB {
    std::string id;
    std::vector<char> source_id;
    std::string target_id;
    bool is_on{};

    RobotStateDB() = default;

    explicit RobotStateDB(const RobotState& msg) {
        id = msg.metadata.id;
        for (const auto &c  :msg.metadata.source_id) {
            source_id.emplace_back(c);
        }
        target_id = msg.metadata.target_id;
        is_on = msg.state == RobotState::E_ON;
    }

    RobotState toRobotState() {
        RobotState robot_state;
        robot_state.metadata.id = id;
        for (const auto &c: source_id) {
            robot_state.metadata.source_id += c;
        }
        robot_state.metadata.target_id = target_id;
        robot_state.state = is_on ? RobotState::E_ON : RobotState::E_OFF;
        return robot_state;
    }
};
