//
// Created by yossi on 2021-04-28.
//

#pragma once

#include <string>
#include <utility>
#include "ThirdParty/nlohmann/json.hpp"

#include "common_helpers.hpp"

using json = nlohmann::json;
using namespace std::string_literals;

struct MetaData {
    std::string id;
    std::string source_id;
    std::string target_id;

    explicit MetaData(std::string id = "", std::string sourceId = "", std::string targetId = "")
            : id(std::move(id)),
              source_id(std::move(sourceId)),
              target_id(std::move(targetId)) {}

    [[nodiscard]] json to_json() const {
        json json_object;
        json_object["id"] = id;
        json_object["sourceId"] = source_id;
        json_object["targetId"] = target_id;
        return json_object;
    }

    [[maybe_unused]] static MetaData from_json_string(std::string json_string) {
        return from_json_object(json::parse(json_string));
    }

    static MetaData from_json_object(json json_object) {
        common_helpers::ensureObjectHasKeys("MetaData",
                                            json_object,
                                            {"id", "sourceId", "targetId"});

        MetaData result(json_object["id"],
                        json_object["sourceId"],
                        json_object["targetId"]);
        return result;
    }
};
