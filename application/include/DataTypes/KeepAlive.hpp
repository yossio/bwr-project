//
// Created by yossi on 2021-04-28.
//

#pragma once

#include <utility>

#include "MetaData.hpp"
#include "ThirdParty/nlohmann/json.hpp"

struct KeepAlive {
    MetaData metadata;
    double unix_timestamp{};

    [[nodiscard]] json to_json() const {
        json json_object;
        json_object["metadata"] = metadata.to_json();
        json_object["unix_timestamp"] = unix_timestamp;
        return json_object;
    }

    explicit KeepAlive(MetaData metadata = MetaData(), double unixTimestamp = 0)
            : metadata(std::move(metadata)),
              unix_timestamp(unixTimestamp) {}

    static KeepAlive from_json_string(const std::string &json_string) {
        return from_json_object(json::parse(json_string));
    }

    static KeepAlive from_json_object(json json_object) {
        common_helpers::ensureObjectHasKeys("KeepAlive", json_object,
                                            {"metadata", "unix_timestamp"});

        KeepAlive result(
                MetaData::from_json_object(json_object["metadata"]),
                json_object["unix_timestamp"]
        );
        return result;
    }
};

struct KeepAliveDB {
    int db_id;
    std::string id;
    std::string source_id;
    std::string target_id;
    double unix_timestamp{};

    explicit KeepAliveDB(const KeepAlive& msg) {
        db_id = -1;
        id = msg.metadata.id;
        source_id = msg.metadata.source_id;
        target_id = msg.metadata.target_id;
        unix_timestamp = msg.unix_timestamp;
    }
};
