//
// Created by yossi on 2021-04-28.
//

#pragma once

#include "DataTypes/MetaData.hpp"

struct CommandResult {
    MetaData metadata;
    std::string commandId;
    enum RESULT {
        E_ACCEPTED,
        E_REJECTED
    } result{};
    std::string message;

    [[nodiscard]] std::string get_result() const {
        switch (result) {
            case E_ACCEPTED:
                return "Accepted";
            case E_REJECTED:
                return "Rejected";
            default:
                return "UNKNOWN";
        }
    }

    [[nodiscard]] json to_json() const {
        json json_object;
        json_object["metadata"] = metadata.to_json();
        json_object["commandId"] = commandId;
        json_object["message"] = message;
        json_object["result"] = get_result();
        return json_object;
    }

    static CommandResult from_json_string(std::string json_string) {
        return from_json_object(json::parse(json_string));
    }

    static CommandResult from_json_object(json json_object) {
        CommandResult result;
        common_helpers::ensureObjectHasKeys(
                "CommandResult", json_object,
                {
                        "metadata",
                        "commandId",
                        "result",
                        "message",
                });
        result.metadata = MetaData::from_json_object(json_object["metadata"]);
        result.commandId = json_object["commandId"];
        result.message = json_object["message"];
        if (json_object["result"] == "Accepted") {
            result.result = E_ACCEPTED;
        } else if (json_object["result"] == "Rejected") {
            result.result = E_REJECTED;
        }
        return result;
    }
};

struct CommandResultDB {
    int db_id;
    std::string id;
    std::string source_id;
    std::string target_id;
    uint8_t result;
    std::string commandId;
    std::string message;

    explicit CommandResultDB(const CommandResult &msg) {
        db_id = -1;
        id = msg.metadata.id;
        source_id = msg.metadata.source_id;
        target_id = msg.metadata.target_id;
        result = msg.result;
        commandId = msg.commandId;
        message = msg.message;
    }
};
