//
// Created by yossi on 2021-04-29.
//

#pragma once

#include "MetaData.hpp"
#include "ThirdParty/nlohmann/json.hpp"

struct Robots {
    using json = nlohmann::json;
    MetaData metadata;
    std::vector<std::string> ids;

    [[nodiscard]] json to_json() const {
        json json_object;
        json_object["metadata"] = metadata.to_json();
        json_object["robots"] = json::array();
        for (const auto &id: ids) {
            json_object["robots"].push_back(id);
        }
        return json_object;
    }
};
