//
// Created by yossi on 2021-04-29.
//

#pragma once

#include "MetaData.hpp"

struct TaskStatus {
    MetaData metadata;
    std::string commandId;
    std::string taskId;
    enum Status {
        E_NONE,
        E_IN_PROGRESS,
        E_COMPLETED,
        E_ABORTED,
        E_CANCELED
    } status{};
    std::string message;

    [[nodiscard]] std::string get_status_string() const {
        switch (status) {
            case E_NONE:
                return "None";
            case E_IN_PROGRESS:
                return "InProgress";
            case E_COMPLETED:
                return "Completed";
            case E_ABORTED:
                return "Aborted";
            case E_CANCELED:
                return "Cancelled";
            default:
                return "UNKNOWN";
        }
    }

    [[nodiscard]] json to_json() const {
        json json_object;
        json_object["metadata"] = metadata.to_json();
        json_object["commandId"] = commandId;
        json_object["taskId"] = taskId;
        json_object["message"] = message;
        json_object["status"] = get_status_string();
        return json_object;
    }

    static TaskStatus from_json_string(std::string json_string) {
        return from_json_object(json::parse(json_string));
    }

    static TaskStatus from_json_object(json json_object) {
        TaskStatus result;
        try {
            common_helpers::ensureObjectHasKeys(
                    "TaskStatus", json_object,
                    {
                            "metadata",
                            "commandId",
                            "taskId",
                            "status",
                            "message",
                    });
        }
        catch (std::exception &e) {
            result.metadata.id = e.what();
            return result;
        }
        result.metadata = MetaData::from_json_object(json_object["metadata"]);
        result.commandId = json_object["commandId"];
        result.taskId = json_object["taskId"];
        result.message = json_object["message"];

        result.status = E_NONE;
        if (json_object["status"] == "InProgress") {
            result.status = E_IN_PROGRESS;
        } else if (json_object["status"] == "Completed") {
            result.status = E_COMPLETED;
        } else if (json_object["status"] == "Aborted") {
            result.status = E_ABORTED;
        } else if (json_object["status"] == "Cancelled") {
            result.status = E_CANCELED;
        }
        return result;
    }

};
