//
// Created by yossi on 2021-04-28.
//

#pragma once

#include "MetaData.hpp"

struct Command {
    MetaData metadata;
    enum Type {
        E_NONE,
        E_START,
        E_STOP
    } type{};
    std::string taskId;

    [[nodiscard]] std::string get_type() const {
        switch (type) {
            case E_NONE:
                return "None";
            case E_START:
                return "Start";
            case E_STOP:
                return "Stop";
            default:
                return "UNKNOWN";
        }

    }

    [[nodiscard]] json to_json() const {
        json json_object;
        json_object["metadata"] = metadata.to_json();
        json_object["type"] = get_type();
        json_object["taskId"] = taskId;
        return json_object;
    }

    static Command from_json_string(std::string json_string) {
        return from_json_object(json::parse(json_string));
    }

    static Command from_json_object(json json_object) {
        Command result;
        common_helpers::ensureObjectHasKeys(
                "Command", json_object,
                {
                        "metadata",
                        "taskId",
                        "type",
                });
        result.metadata = MetaData::from_json_object(json_object["metadata"]);
        result.type = E_NONE;
        if (json_object["type"] == "Start") {
            result.type = E_START;
        } else if (json_object["type"] == "Stop") {
            result.type = E_STOP;
        }
        result.taskId = json_object["taskId"];
        return result;
    }
};

struct CommandDB {
    int db_id;
    std::string id;
    std::string source_id;
    std::string target_id;
    uint8_t type;
    std::string taskId;

    explicit CommandDB(const Command &msg) {
        db_id = -1;
        id = msg.metadata.id;
        source_id = msg.metadata.source_id;
        target_id = msg.metadata.target_id;
        type = msg.type;
        taskId = msg.taskId;
    }
};
